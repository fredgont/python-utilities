CREATE TABLE annonce_to_visit(
  url TEXT,
  tel TEXT,
  seen NUM,
  date_seen NUM,
  emailed NUM,
  date_emailed NUM,
  phoned NUM,
  date_phoned NUM,
  code_postal TEXT,
  prix INT,
  description TEXT
);
