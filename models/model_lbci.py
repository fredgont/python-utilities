from flask import Flask
from flask.ext.sqlalchemy import SQLAlchemy,Model
from sqlalchemy import orm, Table, Column, Integer, String, Boolean, DateTime, Numeric
from controllers import app
from database import Db

class LocationToVisit(Db.Model):

    __tablename__ = "location_to_visit"
    url = Db.Column(String,primary_key=True, nullable=False) 
    tel = Db.Column(String) 
    seen = Db.Column(Boolean) 
    date_seen = Db.Column(DateTime)
    emailed = Db.Column(Boolean) 
    date_emailed = Db.Column(DateTime) 
    phoned = Db.Column(Boolean) 
    date_phoned = Db.Column(DateTime) 
    code_postal = Db.Column(String) 
    loyer = Db.Column(Integer) 
    surface = Db.Column(Integer)
    classe = Db.Column(String) 
    prix_energie_min = Db.Column(Numeric)
    prix_energie_max = Db.Column(Numeric)

    
    def getObjectsUrlLike(self, url):
        return LocationToVisit.query.filter(LocationToVisit.url.like(url+"%")).all()
        
    def to_csv_header(self):
        return "url;tel;seen;date_seen;emailed;date_emailed;phoned;date_phoned;code_postal;loyer;surface;classe;prix_energie_min;prix_energie_max"

    def to_csv_line(self):
        return self.url + ";" +self.tel + ";" +str(self.seen) + ";" +str(self.date_seen) + ";" +str(self.emailed) + ";" +str(self.date_emailed) + ";" +str(self.phoned) + ";" +str(self.date_phoned) + ";" +self.code_postal + ";" +str(self.loyer) + ";" +str(self.surface) + ";" +self.classe + ";" +str(self.prix_energie_min) + ";" +str(self.prix_energie_max)


    def __repr__(self):
        return '<LocationToVisit %r>' % self.url

class AnnonceToVisit(Db.Model):

    __tablename__ = "annonce_to_visit"
    url = Db.Column(String,primary_key=True, nullable=False) 
    tel = Db.Column(String) 
    seen = Db.Column(Boolean) 
    date_seen = Db.Column(DateTime)
    emailed = Db.Column(Boolean) 
    date_emailed = Db.Column(DateTime) 
    phoned = Db.Column(Boolean) 
    date_phoned = Db.Column(DateTime) 
    code_postal = Db.Column(String) 
    prix = Db.Column(Integer) 

    
    def getObjectsUrlLike(self, url):
        return AnnonceToVisit.query.filter(AnnonceToVisit.url.like(url+"%")).all()
        

    def to_csv_header(self):
        return "url;tel;seen;date_seen;emailed;date_emailed;phoned;date_phoned;code_postal;prix"

    def to_csv_line(self):
        return self.url + ";" +self.tel + ";" +str(self.seen) + ";" +str(self.date_seen) + ";" +str(self.emailed) + ";" +str(self.date_emailed) + ";" +str(self.phoned) + ";" +str(self.date_phoned) + ";" +self.code_postal + ";" +str(self.prix)


    def __repr__(self):
        return '<AnnonceToVisit %r>' % self.url


class Links(Db.Model):

    __tablename__ = "links"

    
    url = Db.Column(String, primary_key=True) 
    date = Db.Column(DateTime) 
    seen = Db.Column(Boolean) 
    nb_views = Db.Column(Integer)
    emailed = Db.Column(Boolean) 
    
    def __init__(self,url, date, seen, nb_views, emailed):
        self.url = url
        self.date = date
        self.seen = seen
        self.nb_views= views
        self.emailed = emailed

    def __repr__(self):
        return '<Link %r>' % self.url



class Account:
    email = Db.Column(String, primary_key=True) 
    isOnline = Db.Column(Boolean) 
    numberOfVisit = Db.Column(Integer)
    
    def __init__(self, email):
        self.email = email

    def __repr__(self):
        return '<Account %r>' % self.email
