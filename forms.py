from flask_wtf import Form
from wtforms import HiddenField, StringField, BooleanField, DateField
from wtforms.validators import DataRequired

class LocationToVisitForm(Form):
    url = HiddenField('url', validators=[DataRequired()])
    tel = StringField('tel')
    seen = BooleanField('seen')
    date_seen = DateField('date_deen')
    emailed = BooleanField('emailed')
    date_emailed = DateField('date_emailed')
    phoned = BooleanField('phoned')
    date_phoned = DateField('date_phoned')
    code_postal = StringField('code_postal', validators=[DataRequired()])
    loyer = StringField('loyer', validators=[DataRequired()])
    surface = StringField('surface', validators=[DataRequired()])
    classe = StringField('classe')
    prix_energie_min = StringField('prix_energie_min')
    prix_energie_max = StringField('prix_energie_max')
    
    
    
class AnnonceToVisitForm(Form):
    url = HiddenField('url', validators=[DataRequired()])
    tel = StringField('tel')
    seen = BooleanField('seen')
    date_seen = DateField('date_deen')
    emailed = BooleanField('emailed')
    date_emailed = DateField('date_emailed')
    phoned = BooleanField('phoned')
    date_phoned = DateField('date_phoned')
    code_postal = StringField('code_postal', validators=[DataRequired()])
    prix = StringField('prix', validators=[DataRequired()])
