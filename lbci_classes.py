from enum import Enum
import urllib
import logging
import logging.handlers
logger = logging.getLogger("default")
logger.setLevel(logging.DEBUG)


class  LBCSiteType(Enum):
    ANNONCES='annonces'
    IMMO_LOCATIONS='location'
    IMMO_VENTES='ventes'
    EMPLOI='emploi'
    SPORTHOBBY='sports_hobbies'


"""
Class 
"""
class LBCSiteOptions(object):

    search = {}
    filters = {}
    def __init__(self, search, filters):
        logger.debug("LBCSiteOptions constructor : search {0}".format(search))
        logger.debug("LBCSiteOptions constructor : filters {0}".format(filters))
        self.search = search
        self.filters = filters

    def getSearchName(self, key):
        logger.debug("getSearchName {0}".format(key))
        if (self.search != None and self.search.has_key(key) \
          and self.search.get(key) \
          and isinstance(self.search.get(key), dict) \
          and self.search.get(key).has_key('name')):
            value = self.search.get(key).get('name')
            logger.debug('return value description {0}'.format(value))
            return value
        logger.debug('no found value description')
        return None

    def getFilterName(self, key):
        logger.debug("getFilterName {0}".format(key))
        if (self.filters != None and isinstance(self.filters, dict) \
          and self.filters.has_key(key) and self.filters.get(key) != None \
          and isinstance(self.filters.get(key), dict) and self.filters.get(key).has_key('name')):
            value = self.filters.get(key).get('name')
            logger.debug('return value description {0}'.format(value))
            return value
        logger.debug('no found value description')
        return None
        
    def getFilterValueDescription(self, key, value):
        if value.isdigit():
            svalue = float(value)
        else:
            svalue = value

        if self.filters.has_key(key) and self.filters.get(key) != None \
        and isinstance(self.filters.get(key), dict) and self.filters.get(key).has_key('values') \
        and self.filters.get(key).get('values') != None :
            lvalue = self.filters.get(key).get('values').get(svalue)
            logger.debug('return value description {0}'.format(lvalue))
            return lvalue
        logger.debug('no found value description')
        return None


"""
Create the site optiosn and merge with script args
"""
class LBCSiteOptionsFactory(object):

    @staticmethod
    def convertToSiteOptions(options,type ):
        logger.debug("convertToSiteOptions : options {0}".format(options))
        
        site_options = LBCSiteOptionsFactory.createInstance(type)
        logger.debug("convertToSiteOptions : site_options.filters {0}".format(site_options.filters))
        logger.debug("convertToSiteOptions : site_options.search {0}".format(site_options.search))
        
        filters_keys = options.viewkeys() & site_options.filters.viewkeys()
        search_keys= options.viewkeys() & site_options.search.viewkeys()
        
        filters = {}
        searches ={}
        for key in filters_keys:
            logger.debug("convertToSiteOptions : filters_keys {0}".format(key))
            filters[key] = options.get(key)

        for key in search_keys:
            logger.debug("convertToSiteOptions : search_keys {0}".format(key))
            searches[key] = options.get(key)

        if options.get('searches') != None :
            searches['q'] = options.get('searches')

        return LBCSiteOptions(searches, filters)
                    
    @staticmethod
    def createInstance( type ):
        search = {
            'q': {'name': 'Recherche'},
            'location': {'name':'code postal', 'values' : [ 'Nantes 44100', 'Nantes 44000']},
        }
        default_filters = {'q': {'name': 'mots cles'},'location': {'name': 'codes postaux', 'values' : {}}}
        l_filters = {}
        
        # pour le site d'annonces l'axe recherche et location sont complementaire, les location sont des filtres
        if type == LBCSiteType.ANNONCES: 
            search = {'q': {'name': 'Recherche'}}
            l_filters =  {'location': {'name':'code postal', 'values' : { }}}
            
        if type == LBCSiteType.IMMO_LOCATIONS: 
            l_filters =  {
                'ret': {'name': 'Type de location', 'values' : {1: 'Maison', 2: 'Appartement'}},
                'ros' : { 'name': 'numbre de piece min'},
                'sqs' : { 'name': 'surface min', 'values' : {7:  '60m',8:  '70m',9:  '80m'}}}
            

        if type == LBCSiteType.IMMO_VENTES: 
            l_filters =   {
                'ret' : {'name': 'Type de vente', 'values' : {1: 'Maison', 
                                                              2: 'Appartement'
                                                          }},
                'ros' : { 'name': 'numbre de piece min'},
                'roe' : {'name' : 'Pieces max'},
                'sqs' : {'name' : 'Surface min', 'values':{ 0:'0',
                                                            1:'20',
                                                            2:'25',
                                                            3:'30',
                                                            4:'35',
                                                            5:'40',
                                                            6:'50',
                                                            7:'60',
                                                            8:'70',
                                                            9:'80',
                                                            10:'90',
                                                            11:'100',
                                                            12:'110',
                                                            13:'120',
                                                            14:'130',
                                                            15:'140',
                                                            16:'150',
                                                            17:'200',
                                                            18:'300',
                                                            19:'500',
                                                        }},
                'sqe' : {'name' : 'Surface max', 'values' :{ 1:'20',
                                                             2:'25',
                                                             3:'30',
                                                             4:'35',
                                                             5:'40',
                                                             6:'50',
                                                             7:'60',
                                                             8:'70',
                                                             9:'80',
                                                             10:'90',
                                                             11:'100',
                                                             12:'110',
                                                             13:'120',
                                                             14:'130',
                                                             15:'140',
                                                             16:'150',
                                                             17:'200',
                                                             18:'300',
                                                             19:'500',
                                                             20:'Plus de 500',
                                                         }},

                'ps' : {'name' : 'Prix min', 'values' : { 0:'0',
                                                         1:'25 000',
                                                         2:'50 000',
                                                         3:'75 000',
                                                         4:'100 000',
                                                         5:'125 000',
                                                         6:'150 000',
                                                         7:'175 000',
                                                         8:'200 000',
                                                        9:'225 000',
                                                         10:'250 000',
                                                         11:'275 000',
                                                         12:'300 000',
                                                         13:'325 000',
                                                         14:'350 000',
                                                         15:'400 000',
                                                         16:'450 000',
                                                         17:'500 000',
                                                         18:'550 000',
                                                         19:'600 000',
                                                         20:'650 000',
                                                         21:'700 000',
                                                         22:'800 000',
                                                         23:'900 000',
                                                         24:'1 000 000',
                                                         25:'1 100 000',
                                                         26:'1 200 000',
                                                         27:'1 300 000',
                                                         28:'1 400 000',
                                                         29:'1 500 000',
                                                         30:'2 000 000',
                                                     }},
                'pe' : {'name' : 'Prix max', 'values' :{1:'25 000',
                                                        2:'50 000',
                                                        3:'75 000',
                                                        4:'100 000',
                                                        5:'125 000',
                                                        6:'150 000',
                                                        7:'175 000',
                                                        8:'200 000',
                                                        9:'225 000',
                                                        10:'250 000',
                                                        11:'275 000',
                                                        12:'300 000',
                                                        13:'325 000',
                                                        14:'350 000',
                                                        15:'400 000',
                                                        16:'450 000',
                                                        17:'500 000',
                                                        18:'550 000',
                                                        19:'600 000',
                                                        20:'650 000',
                                                        21:'700 000',
                                                        22:'800 000',
                                                        23:'900 000',
                                                        24:'1 000 000',
                                                        25:'1 100 000',
                                                        26:'1 200 000',
                                                        27:'1 300 000',
                                                        28:'1 400 000',
                                                        29:'1 500 000',
                                                        30:'2 000 000',
                                                        31:'Plus de 2 000 000',
                                                    }},
                'bros' : {'name' : 'Chambres min', 'values':{ 1:'1',
                                                              2:'2',
                                                              3:'3',
                                                              4:'4',
                                                              5:'5',
                                                              6:'6',
                                                          }},
                'broe' : {'name' : 'Chambres max', 'values':{ 1:'1',
                                                              2:'2',
                                                              3:'3',
                                                              4:'4',
                                                              5:'5',
                                                              6:'6',
                                                            999999:'Plus de 6',
                                                          }},
            }
            
        if type == LBCSiteType.EMPLOI: 
            l_filters =  { 
                "jobc" : {"name" : "Type de contrat", "values" :{
                          1:"CDD",
                          2:"CDI",
                          3:"Interim",
                          4:"Independant/Freelance",
                          5:"Stage/Alternance",
                      }},
                "jobs" : {"name" : "Niveau d'etudes", "values" :{
                          1:"Sans diplome",
                          2:"BEP/CAP",
                          3:"Employe/Ouvrier specialise/Bac",
                          4:"Technicien/Employe/Bac+2",
                          5:"Agent de maitrise/Bac+3",
                          6:"Ingenieur/Cadre/Bac+5 ou plus",
                    }},
                "jobf" : {"name" : "Secteur d'activite", "values" :{
                          1:"Agriculture",
                          2:"BTP",
                          3:"Commerce/Distribution",
                          4:"Banque/Assurance",
                          5:"Industrie",
                          6:"Immobilier",
                          7:"Public/Collectivite",
                          8:"Sante",
                          9:"Services",
                          10:"Telecom/Internet",
                          11:"Tourisme",
                          12:"Transport",
                    }},
                "jobd" : {"name" : "Fonction", "values" :{
                          1:"Administration/Services generaux",
                          2:"Commercial/Vente",
                          3:"Comptabilite/Gestion/Finance",
                          4:"Conseil/Audit",
                          5:"Direction Generale",
                          6:"Gardiennage/Securite",
                          7:"Hotellerie/Restauration",
                          8:"Informatique",
                          9:"Juridique",
                          10:"Logistique/Achat",
                          11:"Marketing/Communication",
                          12:"Menage/Entretien",
                          13:"Ressources Humaines/Formation",
                          14:"Services a la personne",
                      }},
                "jobt" : {"name" : "Temps plein/Temps partiel", "values" :{
                          1:"Temps plein",
                          2:"Temps partiel",
                      }},
            }
        return LBCSiteOptions( search , dict(default_filters.items() + l_filters.items()))

"""
"""
class LBCSiteQueryFactory(object):
    def __init__(self, site):
        self.mLBCSite = site


    # creeer une requetes selon les elements recherches et les filtres demandes  
  
    def createQueryForSearchAndFilters(self, search_key, search_value, filter_options):
        query = set()
        logger.debug('add search for query {0}'.format(search_value))
        query.add((search_key,search_value))
        for filter_key in filter_options: 
            filter_value = filter_options[filter_key]
            if filter_value != None:
                if isinstance(filter_value, (dict,list, tuple)):
                    for item_value in filter_value:
                        logger.debug('add filter for query {0} {1}'.format(filter_key, item_value))
                        query.add( (filter_key,item_value))
                else:              
                    logger.debug('add filter for query {0} {1}'.format(filter_key, filter_value))
                    query.add( (filter_key,filter_value))
        logger.debug("query {0}".format(query))
        return query

    # creer des ensemble de donnees structures pour construire des URLs
    def getQueries(self):
        #pour chaque element de recherche
        #    si l'element recherche est une collection -> on doit creer autant de requetes que d'elements
        #    pour chaque element de filtre 
        #       si l'element filtre est une collection -> on doit creer autant de requete que d'elements
        queries = []
        options = self.mLBCSite.getOptions()
        logger.debug("LBCSiteQueryFactory.getQueries : site.options.search : {0}".format(options.search))
        logger.debug("LBCSiteQueryFactory.getQueries : site.options.filters : {0}".format(options.filters))
        for search_key in options.search:
            if search_key != None:
                logger.debug("LBCSiteQueryFactory.getQueries : search_key : {0}".format(search_key))
                search_value = options.search.get(search_key)
                if search_value != None:
                    if isinstance(search_value, (dict, list, tuple)):
                        logger.debug("LBCSiteQueryFactory.getQueries : search is not null")                      
                        for item_search in search_value:
                            query = self.createQueryForSearchAndFilters(search_key, item_search, options.filters)
                            logger.debug("LBCSiteQueryFactory.getQueries : add query : {0}".format(query))
                            queries.append(query)
                    else:
                        logger.debug("LBCSiteQueryFactory.getQueries : search is null")
                        query = self.createQueryForSearchAndFilters(search_key, search_value , options.filters)
                        logger.debug("LBCSiteQueryFactory.getQueries : add query : {0}".format(query))
                        queries.append(query)

        return queries



"""
"""
class LBCSiteQuery(object):
    def __init__(self, options ):
        self.options = options

"""
"""
class LBCSite(object):
    # @params: baseUrl url pattern
    # @params: lbc_site_options 
    def __init__(self, baseUrl, lbc_site_options ):
        self.url_placeholder={};
        self.baseUrl = baseUrl
        self.default_options = lbc_site_options
        self.options = LBCSiteOptions({},{})

    def setRegion(self, region):
        self.url_placeholder['region'] = region
    
    def setDepartement(self, departement):
        if departement != "" :
          self.url_placeholder['departement'] = departement + '/'
        else:
          self.url_placeholder['departement'] = ""  
    
    def populateOptions(self, options):
        filters_keys= options.filters.viewkeys() & self.default_options.filters.viewkeys()
        search_keys = options.search.viewkeys() & self.default_options.search.viewkeys()
        for key in filters_keys:
            self.options.filters[key] =  options.filters.get(key)
        for key in search_keys:
            self.options.search[key] = options.search.get(key)
        
    def getOptions(self):
        return self.options

    def getQueryDescription(self, query):
        description = "recherche: {recherche}\n options de filtre {filtre}"
        recherche = set()
        filtre = set()
        for pair in query:
            key = pair[0] 
            value = pair[1] 
            logger.debug("find description for {0} {1}".format(key, value))
            #if key exists then get description of option
            if (key in self.default_options.search) :
                recherche.add(self.default_options.getSearchName(key))                
            if( key in self.default_options.filters) :
                fdesc = "{0} : {1}".format(self.default_options.getFilterName(key), self.default_options.getFilterValueDescription(key, value) )
                #if fdesc == None:
                #    fdesc = value
                filtre.add(fdesc)                
        print(filtre)
        return description.format(recherche=",".join(recherche), filtre=",".join(filtre))

    # get url from values defined in query
    def getUrl(self, query):
        url = self.baseUrl
        args = [];
        for pair in query:
            key = pair[0]              
            #if key exists then its an option to add to base url
            if (key in self.options.search) or ( key in self.options.filters) :
                value = pair[1]
                if value != None:
                    logger.debug('append URL param : {0} = {1}'.format(key,value))
                    args.append(pair)
        
        
        url = url.format(**self.url_placeholder)
        fullurl = url + '?' + urllib.urlencode(args)    
        logger.debug('final URL :  '+fullurl)
        return fullurl


class LBCSiteFactory(object):
    @staticmethod      
    def createInstance( type ):
        options = LBCSiteOptionsFactory.createInstance( type )
        if type == LBCSiteType.IMMO_LOCATIONS: 
            return  LBCSite("https://www.leboncoin.fr/locations/offres/{region}/{departement}", options)

        if type == LBCSiteType.IMMO_VENTES: 
            return  LBCSite("https://www.leboncoin.fr/ventes_immobilieres/offres/{region}/{departement}", options)

        if type == LBCSiteType.ANNONCES: 
            return  LBCSite("https://www.leboncoin.fr/annonces/offres/{region}/{departement}", options)
        
        if type == LBCSiteType.EMPLOI:
            return  LBCSite("https://www.leboncoin.fr/emploi/offres/{region}/{departement}", options)
