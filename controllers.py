from flask import Flask
from flask import g
from flask import Response
from flask import request
from flask import redirect, url_for


import logging

# ####### LOGGER #######
logger = logging.getLogger("default")
logger.setLevel(logging.DEBUG)

formatter = logging.Formatter("%(asctime)s | %(levelname)8s | %(message)s")

stStdout = logging.StreamHandler()
stStdout.setFormatter(formatter)
logger.addHandler(stStdout)


app = Flask(__name__)

app.debug=True




@app.route('/')
def main_page():
    
    return "Welcome!"

@app.route('/getall')
def get_all():
    from models.model_lbci import LocationToVisit, AnnonceToVisit, Links
    locations = LocationToVisit.query.all()
    if len(locations) > 0:
        response = locations[0].to_csv_header()
        for location in locations:
            response = location.to_csv_line() + "\n"
        return 'LISTING  , <br /> response : %s ' % (response)
    return 'NOTHING'


@app.route('/detail/<path:page_url>', methods = ['GET', 'POST'])
def get_detail(page_url):
    from models.model_lbci import Links
    from lbci_classes import LBCSiteType
    from services import LBCILinkGuesser, LBCIFormFactory, LBCIModelFactory
    from flask import render_template
    from lbci_page_analyzer import LBCPageAnalyzer, Annonce, Offer
    from wtforms.ext.appengine.db import model_form
    
    #defaults
    seen = "NOT BOOKMARKED"
    emailed = "NOT BOOKMARKED"
    bookmarked = False
    dateCrawled = "NEVER"
    link_id=-1
    tel=""
    links = Links.query.filter(Links.url.like(page_url+'%')).all()
    
    if links is not None and len(links) > 0 :
        logger.debug("the url has a link in database")
        link = links[0]
        type = LBCILinkGuesser.getTypeOfLink(link.url)
        model = LBCIModelFactory.createInstance(type)
        locations = model.getObjectsUrlLike(link.url)
        data ={}
        mtype = None
        if locations is not None and len(locations) > 0 :
            logger.debug("get data from Database and populate form")
            location = locations[0]
            form = LBCIFormFactory.createInstance(type, obj=location)
            if(type ==  LBCSiteType.IMMO_VENTES or type ==  LBCSiteType.IMMO_LOCATIONS ):
                mtype = 'IMMO' 
            else:
                mtype = 'ANNONCE'     
            bookmarked = True
        else:
            url=link.url 
            class MyFormData:
                def __init__(self, **entries):
                    self.__dict__.update(entries)
                
             #try:
            logger.debug("get data from page and populate form")
            lbci_page_analyzer = LBCPageAnalyzer()
            offer = lbci_page_analyzer.analyze_page(page_url, type)
                
 
            if isinstance(offer, Offer): 
                mtype = 'IMMO' 
                data = {
                    'url': url,
                    'link_id': link_id,
                    'code_postal' :offer.cp,
                    'loyer' : offer.price,
                    'surface' : offer.surface,
                    'classe' : offer.classE,
                    'prix_energie_min' : offer.minPriceEnergy,
                    'prix_energie_max' : offer.maxPriceEnergy
                }
            elif isinstance(offer, Annonce):     
                mtype = 'ANNONCE' 
                data = {
                    'url': url,
                    'link_id': link_id,
                    'code_postal' :offer.cp,
                    'prix' : offer.price,
                }

            logger.debug("mtype {0}".format(mtype))
                
            formData = MyFormData(**data)
            form = LBCIFormFactory.createInstance(type, obj=formData)
        
        return render_template('actionFrame.html', bookmarked=bookmarked,url=page_url, form=form, type=mtype)
                        
        
    else:
        logger.debug("the requested url doesnt exist in database")
        return 'NOT FOUND'
    

@app.route('/update', methods=['POST'])
def update_link():
    from database import Db
    from services import LBCILinkGuesser, LBCIFormFactory, LBCIModelFactory
    response = ''
    if request.method == 'POST' :
        
        type = LBCILinkGuesser.getTypeOfLink(request.form['url'])
        form = LBCIFormFactory.createInstance(type)
        
        
        logger.debug("feed form with data from request")
        
        form.process(request.form)
        logger.debug("data in request : " + ", ".join(request.form))
        logger.debug("data in form : " + ", ".join(form.data))
        if form.validate_on_submit():
            logger.debug("form is submitted and validated")
            logger.debug("feed Model with form")
            location = LBCIModelFactory.createInstance(type)
            logger.debug('factory return class : ' + location.__class__.__name__)
            form.populate_obj(location)
            exists = location.getObjectsUrlLike(request.form['url']+"%")
            logger.debug("Exists in Database ???? ")
            if (exists is not None) and (len(exists) > 0)  :
                logger.debug("found  entries")
                logger.debug("update data in database")
                Db.session.merge(location)
                Db.session.commit()
                #LocationToVisit.query.update(location.__dict__).where(LocationToVisit.url.like(request.form['url']+"%"))
            else:
                Db.session.add(location)
                Db.session.commit()
            logger.debug("return to details pages")
            return redirect(url_for("get_detail", page_url=location.url))
        else :
            logger.error("cannot validate form")
            details = "<table>"
            for errorMessage, fieldName in form.errors.iteritems():
                logger.error("error : {field} = {error}".format(field=fieldName, error=errorMessage))
                details += "<tr><td>{field}</td><td>{error}</td></tr>".format(field=fieldName, error=errorMessage)
            details += "</table>"
            return details
        
    return 'ERROR Link is not known' 


    
@app.route('/report/locations')
def report_locations():
    from models.model_lbci import LocationToVisit
    locations = LocationToVisit.query.all()
    if len(locations) > 0:
        response = locations[0].to_csv_header() + "\n"
        for location in locations:
            response += location.to_csv_line() + "\n"
        return Response(response, mimetype='text/csv', )
    return 'PROBLEM WITH LISTING'

@app.route('/report/annonces')
def report_annonces():
    from models.model_lbci import AnnonceToVisit
    locations = AnnonceToVisit.query.all()
    if len(locations) > 0:
        response = locations[0].to_csv_header() + "\n"
        for location in locations:
            response += location.to_csv_line() + "\n"
        return Response(response, mimetype='text/csv', )
    return 'PROBLEM WITH LISTING'


@app.route('/delete/<path:page_url>')
def delete_page(page_url):
    from services import LBCILinkGuesser, LBCIFormFactory, LBCIModelFactory
    from database import Db
    type = LBCILinkGuesser.getTypeOfLink(page_url)
    model = LBCIModelFactory.createInstance(type)
    logger.debug("GO TO DELETED")
    locations = model.getObjectsUrlLike(page_url)
    for location in locations:
        logger.debug("IS DELETED1")
        Db.session.delete(location)
        Db.session.commit()
        logger.debug("IS DELETED")
    
    return redirect(url_for('get_detail', page_url=page_url))


# ######  DATABASE ######


@app.teardown_appcontext
def close_db(error):
    """Closes the database again at the end of the request."""
    if hasattr(g, 'sqlite_db'):
        g.sqlite_db.close()

