#!/usr/bin/python
#Script from https://gist.github.com/fclairamb/7734934#file-lbca-py

import requests
import re
import sqlite3
import os
import argparse
import gettext
import logging
import logging.handlers
from lbci_classes import *
from datetime import datetime
from bs4 import BeautifulSoup as bs
from gmail_api_lib import sendMessage

logger = logging.getLogger("default")
logger.setLevel(logging.DEBUG)

formatter = logging.Formatter("%(asctime)s | %(levelname)8s | %(message)s")

stStdout = logging.StreamHandler()
stStdout.setFormatter(formatter)
logger.addHandler(stStdout)

lbciDir= os.environ['HOME']+"/.lbci"
logsDir = lbciDir + "/logs"
if not os.path.isdir(logsDir):
    if not os.path.isdir(lbciDir):
        os.mkdir(lbciDir)
    os.mkdir(logsDir)

stLogfile = logging.handlers.RotatingFileHandler(logsDir+'/log', maxBytes=256*1024, backupCount=10)
stLogfile.setFormatter(formatter)
#stLogfile.doRollover()
logger.addHandler(stLogfile)

parser = argparse.ArgumentParser(description="Le bon coin alert generator")

parser.add_argument("-r", metavar="region", dest="region",  help="region",
                    default="pays_de_la_loire")

parser.add_argument("-d", metavar="departement", dest="departement",  help="departement",
                    default="")

parser.add_argument("-s", metavar="searches", dest="searches", nargs="+", help="Searches to perform",
                    default=[])

parser.add_argument("-e", metavar="email-to", dest="email_to", nargs="+", help="Email to send it to",
                    default="fredgont@gmail.com")

parser.add_argument("-f", metavar="email-from", dest="email_from", help="Email to send it from",
                    default="fredgont@gmail.com")

parser.add_argument("-p", metavar="email-pwd", dest="email_pwd", help="Email Password",
                    default="")

parser.add_argument("-u", metavar="email-subject", dest="email_subject", help="Email's subject",
                    default="An article matched your search")

parser.add_argument("--smtp-server", metavar="smtp-server", dest="server", help="SMTP server to use",
                    default="localhost")

parser.add_argument("--smtp-port", metavar="smtp-port", dest="serverPort", help="SMTP server port to use",
                    default="25")

parser.add_argument("--site", metavar="site", dest="site", help="Site part to scan (annonces, locations, ...)",
                    default="25")

parser.add_argument("--server-name", metavar="server-name", dest="serverName", help="Adress of  for Web Server for administration", default="127.0.0.1:5000")


def parse_LBCSiteOption(options, parser):
    
    for option in options:
        default_value = ''
        mnargs = None
        if 'values' in options[option]:
            mnargs="+"
            default_value = str(options[option]['values'])
            logger.debug('add multiple arg to arg parser {0}'.format(option))
            parser.add_argument("--"+option, metavar=option, dest=option, nargs=mnargs, help=options[option]['name']+ " " + default_value)
        else:
            logger.debug('add single arg  to arg parser{0}'.format(option))
            parser.add_argument("--"+option, metavar=option, dest=option, help=options[option]['name']+ " " + default_value)
    



options_site_annonces = LBCSiteOptionsFactory.createInstance(LBCSiteType.ANNONCES)
options_site_locations = LBCSiteOptionsFactory.createInstance(LBCSiteType.IMMO_LOCATIONS)
options_site_ventes = LBCSiteOptionsFactory.createInstance(LBCSiteType.IMMO_VENTES)
options_site_emplois = LBCSiteOptionsFactory.createInstance(LBCSiteType.EMPLOI)

total_options = dict(options_site_annonces.search.items() + 
                     options_site_annonces.filters.items() + 
                     options_site_locations.search.items() +
                     options_site_locations.filters.items() +
                     options_site_ventes.search.items()+ 
                     options_site_ventes.filters.items()+
                     options_site_emplois.search.items() +
                     options_site_emplois.filters.items())

parse_LBCSiteOption(total_options, parser)

args = parser.parse_args()

mLBCSite = None
siteType = None
if args.site == 'annonces':
    siteType = LBCSiteType.ANNONCES
        
if args.site == 'locations':
    siteType = LBCSiteType.IMMO_LOCATIONS

if args.site == 'ventes':
    siteType = LBCSiteType.IMMO_VENTES

if args.site == 'emploi':
    siteType = LBCSiteType.EMPLOI

logger.debug("construct Site {0}".format(siteType))
mLBCSite = LBCSiteFactory.createInstance(siteType)
logger.debug("args passed to script : {0}".format(vars(args)))
site_options_populated = LBCSiteOptionsFactory.convertToSiteOptions(vars(args), siteType)

mLBCSite.setRegion(args.region)
mLBCSite.setDepartement(args.departement)
mLBCSite.populateOptions(site_options_populated)

queryFactory = LBCSiteQueryFactory(mLBCSite)

queries = queryFactory.getQueries()
links = set()
descriptions = set()
for query in queries:
    description = mLBCSite.getQueryDescription(query)
    link = mLBCSite.getUrl(query)
    print "LINK : " + link
    print "DESC : " + description
    links.add(link)
    descriptions.add(description)


linkRegex = re.compile("//www.leboncoin.fr/[a-z0-9_]+/[0-9]{8,12}\\.htm")


# This is for requests handling



#searchToLinks
def searchLinksInUrl(url):
    links = []
    logger.debug("searchLinksInUrl URL : {url}".format(url=url))
    pageSoup = bs(requests.get(url).content)

    # We search all the link
    for i, aTag in enumerate(pageSoup.findAll('a')):
        description = aTag.get('title') 
        href = aTag.get('href')
        if href:
            logger.debug("searchLinksInUrl : analyze url "+href)
            # And perform a link target matching
            if linkRegex.match(href):
                link = { 'href' : "http:"+href, 'title' : description}
                links.append(link)
            else:
                logger.error("URL '"+href+"' is not valid")    
    return links

#pour chaque code postal
#searchesToLinks(locations):
def searchLinksInUrls(urls):
    links = []
    for url in urls:
        for link in searchLinksInUrl(url):
            logger.debug("add link {0}".format(link))
            links.append(link)

    return links

# DB Preparation
db = sqlite3.connect(lbciDir+"/db")
db.execute("""
CREATE TABLE IF NOT EXISTS links (
  url TEXT ,
  date DATETIME,
  seen BOOL DEFAULT 0,
  nb_views INTEGER,
  email TEXT,
  description TEXT,
  emailed BOOL DEFAULT 0,
  PRIMARY KEY(url, email)
);
""")

gettext.install("lbci")

logger.info("Start !")



import smtplib
import ConfigParser
from email.mime.text import MIMEText
# We save all links
to_email = "_".join(list(args.email_to))
for link in searchLinksInUrls(links):
    logger.info( "Saving {0} {1}".format(link, to_email ))
    db.execute("insert or ignore into links ('url','date','email', 'description') values (?,?,?,?);", (link['href'], datetime.now(), to_email, link['title']))
db.commit()
server_bookmark = "http://" + args.serverName
nb = 0
text = '<ul>\n'
logger.info( "Looking up links for  {0} ".format(to_email ))
for rowid, url, description in db.execute("select rowid, url, description from links where email='"+to_email+"' and (emailed=0 or emailed is null);"):
    logger.info("We have new link : {description} : {link}.".format(link=url, description = description.encode("utf8")))
    text += '<li><a href="{url}">{description} (#{id})</a> </li>\n'.format(id=rowid, url=url, description = description.encode("utf8"))
    db.execute("update links set emailed=1 where rowid=?", (rowid,))
    nb += 1
text += '</ul>\n'
if nb > 0:
    text = _(" {nb} annonces pour la recherche '{search}', dans les codes postaux {location} , {criteres}:<br />".format(nb=nb,search=args.searches,location=args.location , criteres=descriptions)) + '\n' + text
    sendMessage(args.email_from, args.email_to, args.email_subject, text)
           
    db.commit()
    #smtp.quit()
else:
        logger.info("We don't have anything to send !")

db.close()

logger.info("End !")

