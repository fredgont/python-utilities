if __name__ == '__main__':
    from controllers import app
    from database import Db
    from models.model_lbci import LocationToVisit, AnnonceToVisit, Links

    print __name__
    print 'ok all initialized'
    
    # Load default config and override config from an environment variable
    if app.config.from_pyfile('config.dev.cfg') :
        print 'run'
        print app.config
        print 'create all tables'
        Db.create_all(app=app)
        print 'binds'
        print Db.get_binds()
        print 'tables'
        print Db.get_tables_for_bind(None)
        
    else:
        print 'cannot run'    
    
