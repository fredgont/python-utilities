# (description, loyer, Surface, classe energie, cout energetique mensuel [ch. calcule] surf * moyenne class energie * pri moyen kw / 12)
#

import urllib
import logging
import logging.config
import requests
import re
import sys
from bs4 import BeautifulSoup as bs
from lbci_classes import LBCSiteType

# LOGGER
logging.config.fileConfig('logging.conf')
logger = logging.getLogger("pageAnalyzer")
logger.setLevel(logging.DEBUG)

formatter = logging.Formatter("%(asctime)s | %(levelname)8s | %(message)s")

stStdout = logging.StreamHandler()
stStdout.setFormatter(formatter)
logger.addHandler(stStdout)

class EnergyCalculator:
    khw_price = 0.1329
    mapE = {
        'A' : { min :1, max :50 },
        'B' : { min :51, max :90 },
        'C' : { min :91, max :150 },
        'D' : { min :151, max :230 },
        'E' : { min :231, max :330 },
        'F' : { min :331, max :450 },
        }
    price = { min:None, max:None}
    
    def __init__(self,mapEntry, surface):
        for entry in self.mapE:
            if mapEntry == entry:
                logger.debug("match energy class %s" % mapEntry)
                self.price[min] = round(surface * self.mapE[entry][min] * self.khw_price / 12, 2)
                self.price[max] = round(surface * self.mapE[entry][max] * self.khw_price / 12, 2)

    def getPrice(self):
        return self.price            


    
class Annonce(object):
    price=0
    cp=""
    def __init__(self, price, cp):
        self.price = price
        self.cp = cp

class Offer(Annonce):
    surface=0
    classE=None
    minPriceEnergy = 0
    maxPriceEnergy = 0
    def __init__(self, price, cp, surface, classE):
        self.price = price
        self.cp = cp
        self.surface = surface
        self.classE = classE
        if self.classE != None:
            logger.debug("calculate energy costs for class %s " % self.classE)
            calc = EnergyCalculator(self.classE, self.surface) 
            self.minPriceEnergy = calc.price[min]
            self.maxPriceEnergy = calc.price[max]

        
class LBCPageAnalyzer(object):
    # main : analyze page to search info 
    def analyze_page(self, url, type):
        offer = None
        logger.info("LBCPageAnalyzer.analyze_page : analyze url %s" % url)
        response = requests.get(url)
        if response.status_code == 200:
            return self.analyze_content(requests.get(url).content, type)
        else:
            offer = Annonce(None, None)        
        return offer

    # main : analyze page to search info 
    def analyze_content(self, content, type):
        offer = None
        pageSoup = bs(content)
        for i, divTag in enumerate(pageSoup.findAll('div', 'lbcParamsContainer')):
            logger.debug("found tag")
            if type == LBCSiteType.IMMO_LOCATIONS:
                offer = self.analyze_offer(divTag)
            if type == LBCSiteType.IMMO_VENTES:
                offer = self.analyze_vente(divTag)
            if type == LBCSiteType.ANNONCES:
                offer = self.analyze_annonce(divTag)
            return offer


    # return Offer object
    def analyze_annonce(self, tag):
        price = int(self.price_cleaner(self.get_info(tag, 'prix')))
        code_postal = self.get_info(tag, 'code postal')
        return Annonce(price, code_postal)


    # return Offer object
    def analyze_offer(self, tag):
        price = int(self.price_cleaner(self.get_info(tag, 'loyer')))
        code_postal = self.get_info(tag, 'code postal')
        surface = int(self.surface_cleaner(self.get_info(tag, 'surface')))
        classE = self.classe_energy_cleaner(self.get_info(tag, 'classe'))
        return Offer(price, code_postal, surface, classE)


    # return Offer object
    def analyze_vente(self, tag):
        price = int(self.price_cleaner(self.get_info(tag, 'prix')))
        code_postal = self.get_info(tag, 'code postal')
        surface = int(self.surface_cleaner(self.get_info(tag, 'surface')))
        classE = self.classe_energy_cleaner(self.get_info(tag, 'classe'))
        return Offer(price, code_postal, surface, classE)

    # extract info with RegExp    
    def extract(self, string, regexp, indexgroup=None):
        ret_value=None 
        if string is None :
            logger.debug("String from extract is None")
            return None
        if not isinstance(string, str):
            logger.error("String from extract is not a STRING")
            return None
        res = regexp.match(string)
        if res is None:
            logger.debug("Cannot extract pattern : %s in %s" % (regexp.pattern, string))
            return None
        if indexgroup is None:
            ret_value = res.group()
        else:
            ret_value=""
            if isinstance(indexgroup, list): 
                for i in indexgroup:    
                    ret_i = res.group(i)
                    logger.debug("RegExp extract value : {0} for group {1} ".format(ret_i, i) )
                    if ret_i != None:
                        ret_value += ret_i
            else:
                ret_value = res.group(indexgroup)
        logger.debug("RegExp extract value : %s " % ret_value)     
        return ret_value

    #define RegExp for price
    def price_cleaner(self, price_str):
        reg = re.compile('[^0-9]+([0-9]{1,10})[^0-9]+', (re.MULTILINE|re.DOTALL))
        return self.extract(price_str.replace(' ',''), reg,1)

    #define RegExp for surface
    def surface_cleaner(self, surface_str):
        reg = re.compile('[^0-9]+([0-9]{1,5})[^0-9]+')
        return self.extract(surface_str.replace('<sup>2</sup>','').replace('m','').replace(' ',''), reg, 1)

    #define RegExp for classe energie
    def classe_energy_cleaner(self, classe_str):
        reg = re.compile('.+>(A|B|C|D|E|F).+', (re.MULTILINE|re.DOTALL))
        return self.extract(classe_str, reg,1)



    # find the desired info in page    
    def get_info(self, tag, infoname):
        logger.debug(" ------------------  %s -- %s ------------------ " % (infoname, tag.name))
        logger.debug("looking for %s in tag %s " % (infoname, tag.name))
        for thTag in tag.findAll('tr'):
            logger.debug("enter in TR tag")
            got_it = False
            for child in thTag.children:
                if child.name == 'th':
                    logger.debug("enter in TH tag ")
                    logger.debug("=> %s" % (child.contents[0].upper()))
                    if infoname.upper() not in child.contents[0].upper(): continue
                    got_it = True
                if child.name == 'td':
                    logger.debug("enter in TD tag")
                    if got_it:
                        if child.contents is not None:
                            logger.debug(infoname + " : have found it")
                            if len(child.contents) == 1:
                                logger.debug("content : " + child.contents[0])
                                return child.contents[0]
                            logger.debug("content : " + str(child.contents))
                            return str(child.contents)
                        else:
                            logger.error("CANNOT PARSE IT!!")
                    else:
                        logger.debug("have NOT found it")
        return None
       
