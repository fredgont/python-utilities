from lbci_classes import LBCSiteType
from models.model_lbci import LocationToVisit, AnnonceToVisit, Links
from forms import LocationToVisitForm, AnnonceToVisitForm
import re

class LBCILinkGuesser(object):
    @staticmethod
    def getTypeOfLink(link):
        
        linkVentesImmoRegex = re.compile("http://www.leboncoin.fr/ventes_immobilieres/[0-9]{8,12}\\.htm")
        linkLocationImmoRegex = re.compile("http://www.leboncoin.fr/locations/[0-9]{8,12}\\.htm")
        linkRegex = re.compile("http://www.leboncoin.fr/[a-z0-9_]+/[0-9]{8,12}\\.htm")
        if linkVentesImmoRegex.match(link):
            return LBCSiteType.IMMO_VENTES
        if linkLocationImmoRegex.match(link):
            return LBCSiteType.IMMO_LOCATIONS
        if linkRegex.match(link):
            return LBCSiteType.ANNONCES


class LBCIFormFactory(object):
    @staticmethod
    def createInstance(type, obj=None):
        if type == LBCSiteType.IMMO_LOCATIONS:
            return LocationToVisitForm(obj=obj)
        if type == LBCSiteType.IMMO_VENTES:
            return LocationToVisitForm(obj=obj)
        if type == LBCSiteType.ANNONCES:
            return AnnonceToVisitForm(obj=obj)


class LBCIModelFactory(object):
    @staticmethod
    def createInstance(type):
        if type == LBCSiteType.IMMO_LOCATIONS:
            return LocationToVisit()
        if type == LBCSiteType.IMMO_VENTES:
            return LocationToVisit()
        if type == LBCSiteType.ANNONCES:
            return AnnonceToVisit()
