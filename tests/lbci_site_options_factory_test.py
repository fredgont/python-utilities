import unittest
from lbci_classes import LBCSiteOptionsFactory, LBCSiteOptions, LBCSiteType


import logging
import logging.handlers
logger = logging.getLogger("default")
logger.setLevel(logging.INFO)

class LBCSiteOptionsFactoryTest(unittest.TestCase):

    def test_create_instance_annonces(self):
        siteOptions = LBCSiteOptionsFactory.createInstance(LBCSiteType.ANNONCES)

        self.assertTrue(isinstance(siteOptions, LBCSiteOptions))
        self.assertEquals(siteOptions.getSearchName('q'),'Recherche')
        self.assertEquals(siteOptions.getSearchName('location'),None)
        self.assertEquals(siteOptions.getFilterName('location'),'code postal')
        self.assertEquals(siteOptions.getFilterName('ret'), None)
        self.assertEquals(siteOptions.getFilterValueDescription('ret', '1'), None)

    def test_create_instance_locations(self):
        siteOptions = LBCSiteOptionsFactory.createInstance(LBCSiteType.IMMO_LOCATIONS)
        self.assertTrue(isinstance(siteOptions, LBCSiteOptions))
        self.assertEquals(siteOptions.getSearchName('location'),'code postal')
        self.assertEquals(siteOptions.getFilterName('ret'), 'Type de location')
        self.assertEquals(siteOptions.getFilterValueDescription('ret', '1'), 'Maison')

    def test_create_instance_ventes(self):
        siteOptions = LBCSiteOptionsFactory.createInstance(LBCSiteType.IMMO_VENTES)

        self.assertTrue(isinstance(siteOptions, LBCSiteOptions))
        self.assertEquals(siteOptions.getSearchName('location'),'code postal')
        self.assertEquals(siteOptions.getFilterName('ret'), 'Type de vente')
        self.assertEquals(siteOptions.getFilterValueDescription('ret', '1'), 'Maison')

    def test_create_instance_emploi(self):
        siteOptions = LBCSiteOptionsFactory.createInstance(LBCSiteType.EMPLOI)
        self.assertTrue(isinstance(siteOptions, LBCSiteOptions))
        self.assertEquals(siteOptions.getSearchName('location'),'code postal')
        self.assertEquals(siteOptions.getFilterName('jobc'), 'Type de contrat')
        self.assertEquals(siteOptions.getFilterValueDescription('jobs', '2'), 'BEP/CAP')
    
    def test_convertToSiteOption(self):
        logger.setLevel(logging.DEBUG)
        options = { 'jobc' : 2, 'test' : 25}
        siteOptions = LBCSiteOptionsFactory.convertToSiteOptions(options, LBCSiteType.EMPLOI)

        self.assertEquals(siteOptions.filters.get('jobc'), 2)
        self.assertEquals(siteOptions.filters.get('test'), None)
