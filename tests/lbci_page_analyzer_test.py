import unittest
from lbci_page_analyzer import LBCPageAnalyzer, Annonce, Offer
from services import *

import logging
import logging.handlers

logger = logging.getLogger("default")
logger.setLevel(logging.DEBUG)


class LcbiPageAnalyzerTest(unittest.TestCase):

    def test_lbci_location_page_analyzer_energy_null(self):
        analyzer = LBCPageAnalyzer()
        pageHandler = open("tests/appartement01.htm", "r")
        print "lecture page tests/appartement01.htm [sans energie]\n"
        offer = analyzer.analyze_content(pageHandler.read(), LBCSiteType.IMMO_LOCATIONS)
        self.assertIsNotNone(offer)
        self.assertTrue(isinstance(offer, Offer))
        self.assertEquals(50, offer.surface)
        self.assertEquals(695, offer.price)
        self.assertEquals(None, offer.classE)
        self.assertEquals("44000", offer.cp)
        self.assertEquals(0, offer.minPriceEnergy)
        self.assertEquals(0, offer.maxPriceEnergy)

    def test_lbci_location_page_analyzer_energy_not_null(self):
        analyzer = LBCPageAnalyzer()
        pageHandler = open("tests/appartement02.htm", "r")
        print "lecture page tests/appartement02.htm [avec energie]\n"
        offer = analyzer.analyze_content(pageHandler.read(), LBCSiteType.IMMO_LOCATIONS)
        self.assertIsNotNone(offer)
        self.assertTrue(isinstance(offer, Offer))
        self.assertEquals(63, offer.surface)
        self.assertEquals(690, offer.price)
        self.assertEquals("D", offer.classE)
        self.assertEquals("44000", offer.cp)
        self.assertEquals(105.36, offer.minPriceEnergy)
        self.assertEquals(160.48, offer.maxPriceEnergy)

    def test_lbci_vente_page_analyzer_energy_not_null(self):
        analyzer = LBCPageAnalyzer()
        pageHandler = open("tests/vente_appartement01.htm", "r")
        print "lecture page tests/vente_appartement01.htm [avec energie]\n"
        offer = analyzer.analyze_content(pageHandler.read(), LBCSiteType.IMMO_VENTES)
        self.assertIsNotNone(offer)
        self.assertTrue(isinstance(offer, Offer))
        self.assertEquals(92, offer.surface)
        self.assertEquals(163000, offer.price)
        self.assertEquals("D", offer.classE)
        self.assertEquals("44300", offer.cp)
        self.assertEquals(153.85, offer.minPriceEnergy)
        self.assertEquals(234.35, offer.maxPriceEnergy)



    def test_lbci_annonce_page_analyzer(self):
        analyzer = LBCPageAnalyzer()
        pageHandler = open("tests/pousset.htm", "r")
        offer = analyzer.analyze_content(pageHandler.read(), LBCSiteType.ANNONCES)
        self.assertIsNotNone(offer)
        self.assertTrue(isinstance(offer, Annonce))
        self.assertFalse(isinstance(offer, Offer))
        self.assertEquals(300, offer.price)
        self.assertEquals("44200", offer.cp)
        
    def test_lbci_annonce_vase_page_analyzer(self):
        analyzer = LBCPageAnalyzer()
        pageHandler = open("tests/vase.htm", "r")
        offer = analyzer.analyze_content(pageHandler.read(), LBCSiteType.ANNONCES)
        self.assertIsNotNone(offer)
        self.assertTrue(isinstance(offer, Annonce))
        self.assertFalse(isinstance(offer, Offer))
        self.assertEquals(3, offer.price)
        self.assertEquals("44000", offer.cp)


if __name__ == '__main__':
    print "run as module"
    unittest.main()