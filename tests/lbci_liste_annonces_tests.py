from bs4 import BeautifulSoup as bs

import unittest
import re
import logging
import logging.handlers

logger = logging.getLogger("default")
logger.setLevel(logging.DEBUG)

class LcbiOfferListingTest(unittest.TestCase):

    def test_links_are_presents(self) :
        pageHandler = open("tests/liste_annonce_appartements.html", "r")
        pageSoup = bs(pageHandler.read())
        allLinks = pageSoup.findAll('a')
        self.assertTrue(len(allLinks) > 0)
        print("Length " + str(len(allLinks)))
        self.assertEquals(len(allLinks), 126)
        mre = re.compile("//www.leboncoin.fr/[a-z0-9_]+/[0-9]{8,12}\\.htm")
        #mfilter = lambda x : mre.match(x) != None
        mlist = map(lambda y: y.get('href'), allLinks)
        print "liens recuperes"
        print(mlist)
        self.assertTrue(len(mlist) > 0)

        print "liens appartement"
        appartementLinks = filter(lambda x : mre.match(str(x)) != None, mlist )
        print(appartementLinks)
        self.assertTrue(len(appartementLinks) > 0)
        # We search all the link
        for link in appartementLinks:
            print  link
    

#
#


if __name__ == '__main__':
    print "run as module"
    unittest.main()