import logging
import argparse


parser = argparse.ArgumentParser(description="Le bon coin alert bookmarker")

parser.add_argument("-f", metavar="config-file", dest="configFile", help="ConfigFile", default="config.cfg")

args = parser.parse_args()



# ####### LOGGER #######
logger = logging.getLogger("default")
logger.setLevel(logging.DEBUG)

formatter = logging.Formatter("%(asctime)s | %(levelname)8s | %(message)s")

stStdout = logging.StreamHandler()
stStdout.setFormatter(formatter)
logger.addHandler(stStdout)

# ######## FLASK WEB SERVER ##########

if __name__ == '__main__':
    from controllers import app
    from database import Db
    
    print __name__
    print 'ok all initialized'
    
    # Load default config and override config from an environment variable
    if app.config.from_pyfile(args.configFile) :
        print 'run'
        print app.config

        app.run(host=app.config.get('HOST'), port=app.config.get('PORT'), debug=True)
    else:
        print 'cannot run'    
    
